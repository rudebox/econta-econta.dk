<?php 
/**
* Description: Lionlab references repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

if (have_rows('references', 'options') ) :
?>

<section class="references blue--bg">
	<div class="wrap hpad clearfix flex references__container">
		
		<div class="col-sm-2 references__intro">
			<strong class="references__title">Vores samarbejdspartnere</strong>
		</div>
			
		<div class="slider__track--references slider__track--footer is-slider">
		<?php 
			while (have_rows('references', 'options') ) : the_row(); 
				$logo = get_sub_field('references_logo');
		?>

			<div class="references__item">
				<img src="<?php echo esc_url($logo['url']); ?>" alt="<?php echo $logo['alt']; ?>">	
			</div>

		<?php endwhile; ?>
			
		</div>
	</div>
</section>
<?php endif; ?>