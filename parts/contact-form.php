<section class="contact padding--both">
	<div class="wrap hpad clearfix">
		<div class="row flex flex--wrap">
			<div class="col-sm-6 contact__info">
				<?php if (have_rows('footer_contact', 'options') ) : while (have_rows('footer_contact', 'options') ) :
					the_row(); 

					$icon = get_sub_field('footer_icon');
					$link = get_sub_field('footer_link');
					$link_text = get_sub_field('link_text');
				?>
					<div class="contact__links">	
						<a class="center contact__btn" href="<?php echo esc_url($link); ?>"><div data-aos="zoom-in" class="btn--contact contact__btn"><?php echo file_get_contents(esc_url($icon['url'])); ?></div></a>
						<a class="center contact__link" href="<?php echo esc_url($link); ?>"><?php echo esc_html($link_text); ?></a>
					</div>
				<?php endwhile; endif; ?>
			</div>

			<div class="col-sm-6 contact__form">
				<?php gravity_form( 3, $display_title = true, $display_description = true, $display_inactive = false, $field_values = null, $ajax = false, 2, $echo = true ); ?>
			</div>
		</div>
	</div>
</section>