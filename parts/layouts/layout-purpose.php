<?php 
/**
* Description: Lionlab purpose repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');
$sub_title = get_sub_field('sub_header');
$text = get_sub_field('text');

//counter
$i=0;
?>

<section class="purpose gray--bg padding--<?php echo esc_html($margin); ?>">
	<div class="wrap hpad clearfix">
		<h2><?php echo esc_html($title); ?></h2>
		<h6 class="purpose__subtitle"><?php echo esc_html($sub_title); ?></h6>

		<?php if ($text) : ?>
		<div class="row">
			<div class="col-sm-6 purpose__intro"><?php echo esc_html($text); ?></div>
		</div>
		<?php endif; ?>

		
		<div class="row">

			<div class="col-sm-7 col-md-8 purpose__col">
				<div class="row">
					<?php 
						if (have_rows('purpose_textbox') ) :
						while (have_rows('purpose_textbox') ) : the_row();
							$title = get_sub_field('purpose_title');
							$text = get_sub_field('purpose_text');

						$i++;

						if ($i === 2) :

							$anim  = 'data-aos-delay=50';

						elseif ($i === 3) :

							$anim  = 'data-aos-delay=150';

						elseif ($i === 4) :						

							$anim  = 'data-aos-delay=300';

						endif;
					 ?>

					 <div class="col-sm-6 purpose__textbox">
					 	<h3 class="purpose__textbox--title"><?php echo $title; ?><span data-aos="fade-in">.</span></h3>
					 	<p data-aos="fade-right" <?php echo esc_html($anim); ?>><?php echo esc_html($text); ?></p>
					 </div>

					<?php endwhile; endif; ?>
				</div>
			</div>
				
			<?php 
				$return_title = get_sub_field('purpose_return_title');
				$return_text = get_sub_field('purpose_return_text');
			 ?>

			<div class="col-sm-4 col-md-3 col-sm-offset-1 purpose__return">
				<h4 class="purpose__return--title"><?php echo esc_html($return_title); ?></h4>
				<?php echo $return_text; ?>
			</div>

		</div>
	</div>
</section>
