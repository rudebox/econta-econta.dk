<?php 
/**
* Description: Lionlab video field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');
$poster = get_sub_field('video_poster');
?>

<section class="video blue--bg padding--<?php echo esc_html($margin); ?>">
	<div class="video__container" style="background-image: url(<?php echo esc_url($poster['url']); ?>);">
		
		<?php 
		/**
		 * Video
		 **/ 
		$video_mp4 = get_sub_field('mp4');
		$video_ogv = get_sub_field('ogv');
		$video_webm = get_sub_field('webm');
		$title = get_sub_field('video_title');
		$text = get_sub_field('video_text');
		$link = get_sub_field('video_link');

		//cta
		$cta_text = get_sub_field('video_cta_text');
		$cta_mail = get_sub_field('video_cta_mail');
		$cta_subject = get_sub_field('video_cta_subject');
		?>

		
		
		<video id="video" class="video__player" autoplay preload="auto" loop muted="muted" volume="0" poster="">
			<source src="<?php echo $video_mp4; ?>" type="video/mp4" codecs="avc1, mp4a">
			<source src="<?php echo $video_ogv; ?>" type="video/ogg" codecs="theora, vorbis">
			<source src="<?php echo $video_webm; ?>" type="video/webm" codecs="vp8, vorbis">
			Din browser understøtter ikke HTML5 video. Opgrader venligst din browser.
		</video>

		<div class="video__inner slider__text wrap hpad clearfix">
			<div class="row">
				<div class="col-sm-6">
					<h2 class="slider__title h1"><?php echo esc_html($title); ?></h2>
					<p class="video__text"><?php echo esc_html($text); ?></p>
					<a class="btn btn--red" href="<?php echo esc_url($link); ?>">Læs mere</a>
					<a class="btn btn--hollow" href="mailto:<?php echo esc_html($cta_mail); ?>?subject=<?php echo esc_html($cta_subject); ?>"><?php echo esc_html($cta_text); ?></a>
				</div>
			</div>
		</div> 


	</div>
</section>