<?php 
/**
* Description: Lionlab partners repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');

if (have_rows('partner') ) :
?>

<section class="partners blue--bg padding--<?php echo esc_html($margin); ?>">
	<div class="wrap hpad partners__container">
		<h2 class="partners__heading center"><?php echo esc_html($title); ?></h2>
		<div class="row flex flex--wrap">
			<?php 
				while (have_rows('partner') ) : the_row();
					$logo = get_sub_field('partner_logo'); 
 			 ?>

 			 <div class="col-sm-4 partners__item">
 			 	<img src="<?php echo esc_url($logo['sizes']['medium']); ?>" alt="<?php echo $logo['alt']; ?>">
 			 </div>
 			<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>