<?php 
/**
* Description: Lionlab link-boxes repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');
$text = get_sub_field('text');

if (have_rows('linkbox') ) :

//counter
$i=0;
?>

<section class="link-boxes <?php echo $bg; ?>--bg padding--<?php echo $margin; ?>">
	<div class="wrap hpad">
		<h2 class="link-boxes__header center"><?php echo esc_html($title); ?></h2>
		<div class="row flex flex--wrap clearfix link-boxes__row">
		<?php if ($text) : ?>
		<div class="center link-boxes__text col-sm-8 col-sm-offset-2"><?php echo esc_html($text); ?></div>
		<?php endif; ?>
			<?php while (have_rows('linkbox') ) : the_row(); 
				$title = get_sub_field('title');
				$text = get_sub_field('text');
				$icon = get_sub_field('icon');
				$link = get_sub_field('link');
				$link_text = get_sub_field('link_text');

				$image_id = $icon['ID'];

				$i++;

				if ($i === 2) :

					$anim  = 'data-aos-delay=200';

				elseif ($i === 3) :

					$anim  = 'data-aos-delay=350';

				endif;
			?>

			<div class="col-sm-4 link-boxes__item center" <?php echo $anim; ?> data-aos="fade-in">
				<?php if ($icon) : ?>
				<?php echo zen_inline_if_svg( $image_id, 'url' ); ?>
				<?php endif; ?>
				<h3 class="link-boxes__title"><?php echo esc_html($title); ?></h3>
				<p><?php echo esc_html($text); ?></p>
				<?php if ($link) : ?>
				<a class="link-boxes__link" href="<?php echo esc_url($link); ?>"><?php echo esc_html($link_text); ?></a>
				<?php endif; ?>
			</div>
			<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>