<?php 
/**
* Description: Lionlab testimonials repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');

if (have_rows('testimonials') ) : ?>
  <section class="testimonials padding--both">
  	<div class="wrap hpad clearfix">
  		<h2 class="center testimonials__heading"><?php echo esc_html($title); ?></h2>
  		<div class="row flex flex--wrap">
  			<?php while (have_rows('testimonials') ) : the_row(); 
          $img = get_sub_field('testimonials_img');
  				$name = get_sub_field('testimonials_title');
  				$text = get_sub_field('testimonials_text');
  			?>

  			<div class="col-sm-6 testimonials__item">
  				<i class="fa fa-quote-right" aria-hidden="true" data-aos="fade-in"></i>
          <div class="testimonials__img-box center">
            <img src="<?php echo esc_url($img['url']); ?>" alt="<?php echo $img['alt']; ?>">
          </div>
          <div class="testimonials__content">
  				  <h6 class="testimonials__title"><?php echo esc_html($name); ?></h6>
  				  <p class="testimonials__quote"><?php echo esc_html($text); ?></p>
          </div>
  			</div>
  			<?php endwhile; ?>
  		</div>
  	</div>
  </section>
  <?php endif; ?>