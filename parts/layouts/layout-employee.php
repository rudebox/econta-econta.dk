<?php 
/**
* Description: Lionlab link-boxes repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');

//counter
$i=0;

if (have_rows('employee') ) :
?>

<section class="employee padding--<?php echo esc_html($margin); ?>">
	<div class="wrap hpad">
		<div class="row flex flex--wrap">
			<?php 
				while (have_rows('employee') ) : the_row();

					$img = get_sub_field('img');
					$name = get_sub_field('name');
					$position = get_sub_field('position');
					$phone = get_sub_field('phone');
					$mail = get_sub_field('mail');
					$linkedin = get_sub_field('linkedin');

					$i++;

					if ($i === 2 || $i === 5 || $i === 8 || $i === 11 || $i === 14  || $i === 17) :

						$anim  = 'data-aos-delay=250';

					elseif ($i === 3 || $i === 6 || $i === 9 || $i === 12 || $i === 15 || $i === 18) :

						$anim  = 'data-aos-delay=400';

					elseif ($i === 4 || $i === 7 || $i === 10 || $i === 13 ) :
						$anim = '';

					endif;
 			 ?>

 			 <div data-aos="fade-in" class="col-sm-4 employee__item <?php echo $i; ?>" <?php echo esc_attr($anim); ?>>
 			 	<img src="<?php echo esc_url($img['url']); ?>" alt="<?php echo esc_attr($img['alt']); ?>
 			 	">
 			 	<div class="employee__content">
	 			 	<h5 class="employee__name"><?php echo esc_html($name); ?></h5>
	 			 	<strong><?php echo esc_html($position); ?></strong>
	 			 	<a class="employee__link" target="_blank" href="<?php echo esc_url($linkedin); ?>"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
	 			 	<a class="employee__link" href="tel:<?php echo esc_html(get_formatted_phone($phone)); ?>"><i class="fa fa-phone" aria-hidden="true"></i> <?php echo esc_html($phone); ?></a>
	 			 	<a class="employee__link" href="mailto:<?php echo esc_html($mail); ?>"><i class="fa fa-envelope" aria-hidden="true"></i> <?php echo esc_html($mail); ?></a>
 			 	</div>
 			 </div>

 			<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>
