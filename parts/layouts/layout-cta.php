<?php 
/**
* Description: Lionlab cta field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');
$text = get_sub_field('text');

//cta settings
$cta_title = get_sub_field('cta_title');
$cta_text = get_sub_field('cta_text');
$cta_mail = get_sub_field('cta_mail');
$cta_mail_text = get_sub_field('cta_mail_text');

?>

<section class="cta <?php echo $bg; ?>--bg">
	<div class="wrap hpad">
		<div class="row flex flex--wrap clearfix">
			<div class="col-sm-6 cta__text">
				<h2><?php echo esc_html($cta_title); ?></h2>
				<?php echo $cta_text; ?>
			</div>	

			<div class="col-sm-6 cta__contact red--bg flex flex--hvalign">
				<a class="btn btn--hollow" href="mailto:<?php echo esc_html($cta_mail); ?>"><?php echo esc_html($cta_mail_text); ?></a>
			</div>
		</div>
	</div>
</section>