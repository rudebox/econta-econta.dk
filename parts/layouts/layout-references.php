<?php 
/**
* Description: Lionlab references repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');

if (have_rows('references_logo') ) :
?>

<section class="references blue--bg">
	<div class="wrap hpad clearfix flex flex--wrap">
		
		<?php if ($title) : ?>
		<div class="col-sm-2">
			<strong class="references__title"><?php echo esc_html($title);  ?></strong>
		</div>
		<?php endif; ?>
		<div class="slider__track--references is-slider">
		<?php 
			while (have_rows('references_logo') ) : the_row(); 
				$logo = get_sub_field('references_img');
		?>

			<div class="references__item">
				<img src="<?php echo esc_url($logo['url']); ?>" alt="<?php echo $logo['alt']; ?>">	
			</div>

		<?php endwhile; ?>
		</div>
		
	</div>
</section>
<?php endif; ?>