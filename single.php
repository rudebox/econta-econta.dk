<?php

/*
 * Template Name: Layouts
 */

get_template_part('parts/header'); the_post(); ?>

<main>
  
  <?php get_template_part('parts/page', 'header');?>

  <?php get_template_part('parts/content', 'layouts'); ?>

  <?php 
    $show = get_field('show');
    if ($show === true) {
      get_template_part('parts/contact', 'form');     
    }     
   ?>

</main>
<?php get_template_part('parts/footer'); ?>
