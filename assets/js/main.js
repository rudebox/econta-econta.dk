jQuery(document).ready(function($) {

  //menu toggle
  $('.nav-toggle').click(function(e) {
    e.preventDefault();
    $('.nav--mobile').toggleClass('is-open');
    $('body').toggleClass('is-fixed');
  });


  //owl slider/carousel
  var owl = $('.slider__track');

  owl.each(function() {
    $(this).children().length > 1;

    $(this).owlCarousel({
        loop: false,
        items: 1,
        autoplay: true,
        // nav: true,
        autplaySpeed: 11000,
        autoplayTimeout: 10000,
        smartSpeed: 250,
        smartSpeed: 2200,
        navSpeed: 2200
        // navText : ["<i class='fa fa-arrow-left' aria-hidden='true'></i>", "<i class='fa fa-arrow-right' aria-hidden='true'></i>"]
    });
  });

  //owl slider/carousel
  var owl = $('.slider__track--references');

  owl.each(function() {
    $(this).children().length > 1;

    $(this).owlCarousel({
        loop: true,
        items: 5,
        autoplay: true,
        autplaySpeed: 5000,
        autoplayTimeout: 4000,
        smartSpeed: 250,
        smartSpeed: 2200,
        navSpeed: 2200,
        responsive : {
          // breakpoint from 0 up
          0 : {
            items: 2
          },
          // breakpoint from 480 up
          480 : {
            items: 3
          },
          // breakpoint from 768 up
          768 : {
              items: 5
          }
      }
    });
  });

  //toggle accordion
  var $acc = $(".accordion");

    $acc.each(function() {
      $(this).click(function() {
        $(this).toggleClass("active");
        $(this).next().toggleClass("show");
    });
  });

  //AOS init  
  AOS.init({
    duration: 1200,
    once: true,
    easing: 'ease'
  })

   $(function() {

      var $el, leftPos, newWidth,
          $mainNav = $(".nav__menu");
      
      $mainNav.append("<li class='nav__underline'></li>");
      var $underLine = $(".nav__underline");
      
      $underLine
          .width($(".is-active a").width())
          .css("left", $(".is-active a").position().left)
          .data("origLeft", $underLine.position().left)
          .data("origWidth", $underLine.width());
          
      $(".nav__menu .nav__item a").not("btn li a").hover(function() { 
          $el = $(this);
          leftPos = $el.position().left;
          newWidth = $el.parent().width();
          $underLine.stop().animate({
              left: leftPos,
              width: newWidth
          });
      }, function() {
          $underLine.stop().animate({
              left: $underLine.data("origLeft"),
              width: $underLine.data("origWidth")
          });    
      });
  });

   (function() {
    var $header = $('.header');
    var $headerHeight = $header.outerHeight();

    $(window).on('load resize scroll', function(e) {
      var scrollTop = $(window).scrollTop();

      if ( scrollTop >= $headerHeight ) {
        $header.addClass('is-fixed');
      } else {
        $header.removeClass('is-fixed');
      }
    });

  })();

});


$('.nav__item--btn').removeClass('nav__item');


window.onload = function() {

  // Video
  var video = document.getElementById("video");

  // Buttons
  var playButton = document.getElementById("play");
  var pauseButton = document.getElementById("pause");

    // Event listener for the play/pause button
  if (playButton) {
    playButton.addEventListener("click", function() {
      if (video.paused == true) {

        // Play the video
        video.play();

        playButton.classList.toggle('is-hidden');
        pauseButton.classList.toggle('is-hidden'); 
      } else {

        // Pause the video
        video.pause();

        playButton.classList.toggle('is-hidden');
        pauseButton.classList.toggle('is-hidden');
      }
    });
  }

  if (pauseButton) {
    pauseButton.addEventListener("click", function() {
      if (video.paused == true) {

        // Play the video
        video.play();

        playButton.classList.toggle('is-hidden');
        pauseButton.classList.toggle('is-hidden');

      } else {

        // Pause the video
        video.pause();
        
        playButton.classList.toggle('is-hidden');
        pauseButton.classList.toggle('is-hidden');
      }
    });
  }

}

 

