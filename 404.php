<?php get_template_part('parts/header'); ?>

<main>
  <?php get_template_part('parts/page', 'header'); ?>

  <section class="wrap hpad clearfix padding--both">

    <p>Vi kunne ikke finde siden du søgte.</p>

    <a class="btn btn--red" href="/">Tilbage</a>

  </section>

</main>

<?php get_template_part('parts/footer'); ?>